import play.PlayJava

name := """acrobsession"""

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayJava)

scalaVersion := "2.11.1"

libraryDependencies ++= Seq(
  javaJdbc,
  cache,
  "org.postgresql" % "postgresql" % "9.3-1101-jdbc41",
  "org.webjars" % "bootstrap" % "3.2.0"
)
