"acrobsession (noun) : An unusual obsession with converting everything in your language to acronyms." (c) http://nws.merriam-webster.com/opendictionary

ACRobsession is Java (Play Framework) and PostgreSQL (jsonb) backend for collecting ACRA (https://github.com/ACRA/acra) reports.
