# created_at index

# --- !Ups

CREATE INDEX created_at_idx on reports (created_at DESC);

# --- !Downs

DROP INDEX created_at_idx;
