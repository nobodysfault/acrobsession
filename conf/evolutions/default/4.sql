# created_at index

# --- !Ups

CREATE INDEX android_version_json_idx ON reports USING gin ((json->'ANDROID_VERSION'));

# --- !Downs

DROP INDEX android_version_json_idx;
