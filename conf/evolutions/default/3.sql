# created_at index

# --- !Ups

CREATE INDEX json_idx ON reports USING gin (json);
CREATE INDEX app_version_name_json_idx ON reports USING gin ((json->'APP_VERSION_NAME'));

# --- !Downs

DROP INDEX json_idx;
DROP INDEX app_version_name_json_idx;
