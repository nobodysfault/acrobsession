# reports

# --- !Ups

CREATE TABLE reports (
    id serial PRIMARY KEY,
    created_at timestamp NOT NULL,
    json jsonb NOT NULL
);

# --- !Downs

DROP TABLE reports;
