package controllers;

import actions.BasicAuth;
import com.fasterxml.jackson.databind.JsonNode;
import models.Page;
import models.Report;
import play.Logger;
import play.data.DynamicForm;
import play.data.Form;
import play.db.DB;
import play.mvc.BodyParser;
import play.mvc.Controller;
import play.mvc.Result;
import play.twirl.api.Html;
import views.html.list;
import views.html.show;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@BasicAuth
public class Reports extends Controller {
    public static Result list(
        long page,
        String packageName,
        String appVersionName,
        String androidVersion,
        String logcatStackTraceFilter
    ) {
        Page<Report> reportPage;
        try (Connection connection = DB.getConnection()) {
            reportPage = Report.page(
                connection,
                page,
                20,
                packageName,
                appVersionName,
                androidVersion,
                logcatStackTraceFilter
            );
        } catch (SQLException e) {
            Logger.error(e.getMessage(), e);
            return internalServerError(e.getMessage());
        }

        return ok(
            list.render(reportPage)
        );
    }

    public static Result show(Long id) {
        try {
            return ok(show.render(Report.findById(DB.getConnection(), id)));
        } catch (SQLException e) {
            Logger.error(e.getMessage(), e);
            return internalServerError(e.getMessage());
        }
    }

    @BodyParser.Of(BodyParser.TolerantJson.class)
    public static Result save() {
        final JsonNode request = request().body().asJson();
        if (request == null || request.toString().length() == 0) {
            return badRequest("Empty request");
        }

        try (Connection connection = DB.getConnection()) {
            if (Report.save(connection, request.toString())) {
                return ok();
            }
            return internalServerError();
        } catch (SQLException e) {
            Logger.error("Ooops", e);
            return internalServerError(e.getMessage());
        }
    }

    public static Html navbar() {
        DynamicForm requestData = Form.form().bindFromRequest();

        List<String> appVersionNames;
        List<String> androidVersions;
        List<String> packageNames;
        try (Connection connection = DB.getConnection()) {
            appVersionNames = Report.getAppVersionNames(connection);
            androidVersions = Report.getAndroidVersions(connection);
            packageNames    = Report.getPackageNames(connection);
        } catch (SQLException e) {
            Logger.error("Ooops", e);
            return null;
        }

        Map<String, String> appVersionNameMap = new HashMap<>();
        appVersionNameMap.put("", "All app versions");
        for (String name : appVersionNames) {
            appVersionNameMap.put(name, name);
        }

        Map<String, String> androidVersionMap = new HashMap<>();
        androidVersionMap.put("", "All android versions");
        for (String version : androidVersions) {
            androidVersionMap.put(version, version);
        }

        Map<String, String> packageNameMap = new HashMap<>();
        packageNameMap.put("", "All packages");
        for (String name : packageNames) {
            packageNameMap.put(name, name);
        }

        return views.html.navbar.render(requestData, packageNameMap, appVersionNameMap, androidVersionMap);
    }
}
