package actions;

import com.typesafe.config.Config;
import com.typesafe.config.ConfigFactory;
import play.api.Play;
import play.libs.F;
import play.mvc.Http;
import play.mvc.Result;

import javax.xml.bind.DatatypeConverter;

public class BasicAuthAction extends play.mvc.Action.Simple {
    private static final String AUTHORIZATION    = "authorization";
    private static final String WWW_AUTHENTICATE = "WWW-Authenticate";
    private static final String REALM            = "Basic realm=\"ACRobsession\"";

    @Override
    public F.Promise<Result> call(Http.Context ctx) throws Throwable {
        final String authHeader = ctx.request().getHeader(AUTHORIZATION);
        if (authHeader == null) {
            ctx.response().setHeader(WWW_AUTHENTICATE, REALM);
            return F.Promise.pure(unauthorized());
        }

        String auth          = authHeader.substring(6);
        byte[] decodedAuth   = DatatypeConverter.parseBase64Binary(auth);
        String[] credentials = new String(decodedAuth, "UTF-8").split(":");

        if (credentials.length != 2) {
            return F.Promise.pure(unauthorized());
        }

        String username = credentials[0];
        String password = credentials[1];

        Config config = ConfigFactory.load();

        boolean isAuthorized = username.equals(config.getString("basic_auth.username")) &&
            password.equals(config.getString("basic_auth.password"))
        ;

        return isAuthorized ? delegate.call(ctx) : F.Promise.pure(unauthorized());
    }
}
