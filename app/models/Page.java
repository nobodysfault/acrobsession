package models;

import java.util.List;

public class Page<T> {
    private final long pageSize;
    private final long totalRowCount;
    private final long pageIndex;
    private final List<T> list;

    public Page(List<T> data, long total, long page, long pageSize) {
        this.list          = data;
        this.totalRowCount = total;
        this.pageIndex     = page;
        this.pageSize      = pageSize;
    }

    public long getTotalRowCount() {
        return totalRowCount;
    }

    public long getPageIndex() {
        return pageIndex;
    }

    public List<T> getList() {
        return list;
    }

    public boolean hasPrev() {
        return pageIndex > 0;
    }

    public boolean hasNext() {
        return (totalRowCount / pageSize) >= (pageIndex + 1);
    }

    public String getDisplayXtoYofZ() {
        long start = list.isEmpty() ? 0 : pageIndex * pageSize + 1;
        long end   = list.isEmpty() ? 0 : start + Math.min(pageSize, list.size()) - 1;
        return start + " to " + end + " of " + totalRowCount;
    }
}
